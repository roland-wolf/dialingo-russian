# dialingo-russian

Russian for German speakers. 
Provides texts, audio, translations and graphics. 

![TOC](screenshots/toc.png)

![Chapter 1](screenshots/chapter.png)

Build requirements: Qt5, cmake, c++ compiler

To build and run under Linux:

    git clone https://gitlab.com/roland-wolf/dialingo-russian.git
    cd dialingo-russian
    git submodule init
    git submodule update
    mkdir build
    cd build
    cmake ..
    make
    cd bin
    ./dplayer
   
    


